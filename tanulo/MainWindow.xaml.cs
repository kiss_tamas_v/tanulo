﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace tanulo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        StreamReader sr;
        string fajl;
        
        string[] input;
        int KERDESDB;
        Kerdes[] kerdesek;
        List<Kerdes> rosszak = new List<Kerdes>();

        int kerdesIndex=0;
        CheckBox[] valaszCBs;

        bool ellenorizve = false;


        public MainWindow()
        {
            
            
            Beolvas(true);
            InitializeComponent();
            if (this.checkBoxKerdes.IsChecked.Value)
            {
                RandomSorrend(kerdesek);
            }
            if (checkBoxDarkMode != null && checkBoxDarkMode.IsChecked == true)
            {
                BuildUI_Dark(kerdesek[kerdesIndex]);
            }
            else
            {
                BuildUI(kerdesek[kerdesIndex]);
            }


            //Properties.Settings.Default.EltaroltRosszak = rosszak;
        }

        //private void Init()
        //{
        //    kerdesIndex = 0;
        //    Beolvas();
        //    if (this.checkBox.IsChecked.Value)
        //    {
        //        RandomSorrend(kerdesek);
        //    }
        //    BuildUI(kerdesek[kerdesIndex]);
        //}

        private void Beolvas(bool ujfajl)
        {
            try
            {
                if (ujfajl)
                {
                    sr = new StreamReader(inputDialog());
                }
                else
                {
                    sr = new StreamReader(fajl);
                }
                
                input = sr.ReadToEnd().Split(new string[] { "#K" }, StringSplitOptions.RemoveEmptyEntries);
                sr.Close();
                KERDESDB = input.Length;
                kerdesek = new Kerdes[KERDESDB];

                for (int i = 0; i < KERDESDB; i++)
                {
                    kerdesek[i] = new Kerdes(input[i]);
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Az program nem találja az input fájlt!\nHelyezze közös könyvtárba az .exe fájllal!","Hiba",MessageBoxButton.OK, MessageBoxImage.Error);
                System.Environment.Exit(0);
            }            
        }

        private void BuildUI(Kerdes kerdes)
        {
            checkBoxKerdes.Foreground = Brushes.Black;
            if (checkBoxValasz!= null)
            {
                checkBoxValasz.Foreground = Brushes.Black;
            }
            if (checkBoxNehezek != null)
            {
                checkBoxNehezek.Foreground = Brushes.Black;
            }
            if (checkBoxDarkMode != null)
            {
                checkBoxDarkMode.Foreground = Brushes.Black;
            }
            if (label != null)
            {
                label.Foreground = Brushes.Black;
            }            
            mainGrid.Background = Brushes.White;
            this.grid.Children.Clear();

            if (this.checkBoxValasz != null && this.checkBoxValasz.IsChecked.Value)
            {
                RandomSorrend(kerdes.Valaszok);
            }

            this.label.Content = kerdesIndex+1 + " /" + KERDESDB;


            valaszCBs = new CheckBox[kerdes.ValaszDB];
            TextBlock kerdesTB = new TextBlock();
            kerdesTB.Text = kerdes.GetKerdes;
            kerdesTB.TextWrapping = TextWrapping.WrapWithOverflow;
            kerdesTB.Foreground = Brushes.Blue;
            kerdesTB.FontSize = 22;

            StackPanel sp = new StackPanel();
            for (int i = 0; i < kerdes.ValaszDB; i++)
            {
                valaszCBs[i] = new CheckBox();
                //valaszCBs[i].Content = kerdes.Valaszok[i];
                TextBlock tempTb = new TextBlock();
                tempTb.Text = kerdes.Valaszok[i];
                tempTb.TextWrapping = TextWrapping.WrapWithOverflow;
                tempTb.FontSize = 17;
                valaszCBs[i].Content = tempTb;
                sp.Children.Add(valaszCBs[i]);
            }

            StackPanel sp2 = new StackPanel();
            sp2.Children.Add(kerdesTB);
            sp2.Children.Add(sp);

            this.grid.Children.Add(sp2);


        }

        private void BuildUI_Dark(Kerdes kerdes)
        {
            checkBoxKerdes.Foreground = Brushes.White;
            checkBoxValasz.Foreground = Brushes.White;
            checkBoxNehezek.Foreground = Brushes.White;
            checkBoxDarkMode.Foreground = Brushes.White;
            label.Foreground = Brushes.White;
            mainGrid.Background = new SolidColorBrush(Color.FromRgb(30, 30, 30));

            this.grid.Children.Clear();

            if (this.checkBoxValasz != null && this.checkBoxValasz.IsChecked.Value)
            {
                RandomSorrend(kerdes.Valaszok);
            }

            this.label.Content = kerdesIndex + 1 + " /" + KERDESDB;


            valaszCBs = new CheckBox[kerdes.ValaszDB];
            TextBlock kerdesTB = new TextBlock();
            kerdesTB.Text = kerdes.GetKerdes;
            kerdesTB.TextWrapping = TextWrapping.WrapWithOverflow;
            kerdesTB.Foreground = new SolidColorBrush(Color.FromRgb(78, 201, 176));
            kerdesTB.FontSize = 22;
            

            StackPanel sp = new StackPanel();
            for (int i = 0; i < kerdes.ValaszDB; i++)
            {
                valaszCBs[i] = new CheckBox();
                //valaszCBs[i].Content = kerdes.Valaszok[i];
                TextBlock tempTb = new TextBlock();
                tempTb.Text = kerdes.Valaszok[i];
                tempTb.TextWrapping = TextWrapping.WrapWithOverflow;
                tempTb.FontSize = 17;
                tempTb.Foreground = Brushes.White;
                valaszCBs[i].Content = tempTb;
                //valaszCBs[i].Foreground = Brushes.White;
                sp.Children.Add(valaszCBs[i]);
                
            }

            StackPanel sp2 = new StackPanel();
            sp2.Children.Add(kerdesTB);
            sp2.Children.Add(sp);

            this.grid.Children.Add(sp2);
            //this.grid.Background = Brushes.Gray;


        }

        private void RandomSorrend(object[] tomb)
        {
            if (tomb.Length>2)
            {
                Random rnd = new Random();
                for (int i = 0; i < tomb.Length * 3; i++)
                {
                    int index1 = rnd.Next(tomb.Length);
                    int index2 = 0;
                    do
                    {
                        index2 = rnd.Next(tomb.Length);
                    }
                    while (index2 == index1);

                    object temp = tomb[index1];
                    tomb[index1] = tomb[index2];
                    tomb[index2] = temp;
                }
            }
            
        }

        private void Reset()
        {
            kerdesIndex = 0;
            rosszak = new List<Kerdes>();
            input = null;
            kerdesek = null;

            ellenorizve = false;
            this.button.Content = "Ellenőrzés";

            Beolvas(false);
            if (this.checkBoxKerdes.IsChecked.Value)
            {
                RandomSorrend(kerdesek);
            }
            if (checkBoxDarkMode!=null && checkBoxDarkMode.IsChecked == true)
            {
                BuildUI_Dark(kerdesek[kerdesIndex]);
            }
            else
            {
                BuildUI(kerdesek[kerdesIndex]);
            }

            //if (rosszak.Count >0)
            //{
            //    checkBoxNehezek.IsEnabled = true;
            //}
        }

        private void MegoldasGUI()
        {
            int rossz = 0;
            for (int i = 0; i < valaszCBs.Length; i++)
            {
                valaszCBs[i].IsEnabled = false;
                foreach (var item in kerdesek[kerdesIndex].Megoldas)
                {
                    if (((TextBlock)valaszCBs[i].Content).Text.ToString() == item.ToString())
                    {
                        ((TextBlock)valaszCBs[i].Content).Foreground = Brushes.Green;
                    }
                    if (!valaszCBs[i].IsChecked.Value && ((TextBlock)valaszCBs[i].Content).Text.ToString() == item.ToString())
                    {
                        rossz++;
                    }
                }
            }

            if (rossz > 0)
            {
                rosszak.Add(kerdesek[kerdesIndex]);
            }
            //else if (checkBoxNehezek.IsChecked.Value)
            //{
            //    rosszak.Remove(kerdesek[kerdesIndex]);
            //}

            ellenorizve = true;
            this.button.Content = "Következő";
        }
       
        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (ellenorizve==false)
            {
                MegoldasGUI();
            }
            else if (kerdesIndex+1 < kerdesek.Length && ellenorizve == true)
            {
                kerdesIndex++;
                if (checkBoxDarkMode != null && checkBoxDarkMode.IsChecked == true)
                {
                    BuildUI_Dark(kerdesek[kerdesIndex]);
                }
                else
                {
                    BuildUI(kerdesek[kerdesIndex]);
                }
                ellenorizve = false;
                this.button.Content = "Ellenőrzés";
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Nincs több kérdés\nEredménye: " + (kerdesek.Length - rosszak.Count) + "/" + kerdesek.Length + " (" + Math.Round(((kerdesek.Length - rosszak.Count)/(kerdesek.Length+0.0))*100) + " %)" );
                if (result == MessageBoxResult.OK)
                {
                    //kerdesIndex = 0;
                    //BuildUI(kerdesek[kerdesIndex]);
                    Reset();
                }
                
            }
        }

        private void button_Click_input(object sender, RoutedEventArgs e)
        {
            //inputDialog();
            Beolvas(true);
            Reset();
        }

        private string inputDialog()
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".txt";
            dlg.Filter = "TEXT Files (*.txt)|*.txt";


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();
            
            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // return document name 
                fajl = dlg.FileName.ToString();
                return fajl;
            }
            return "input.txt";
        }

        private void checkBoxKerdes_Checked(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void checkBoxDarkMode_Checked(object sender, RoutedEventArgs e)
        {
            if (checkBoxDarkMode.IsChecked == true)
            {
                BuildUI_Dark(kerdesek[kerdesIndex]);
            }
            else
            {
                BuildUI(kerdesek[kerdesIndex]);
            }
        }

        //private void checkBoxNehezek_Checked(object sender, RoutedEventArgs e)
        //{
        //    kerdesek = rosszak.ToArray();
        //    KERDESDB = rosszak.Count;
        //    kerdesIndex = 0;
        //    BuildUI(kerdesek[kerdesIndex]);
        //}

        //private void checkBoxNehezek_Unchecked(object sender, RoutedEventArgs e)
        //{
        //    Reset();
        //}

        private void checkBoxValasz_Checked(object sender, RoutedEventArgs e)
        {
            if (!(sender as CheckBox).IsChecked.Value)
            {
                kerdesek[kerdesIndex].SorrendVisszaallitas();
            }
            if (checkBoxDarkMode != null && checkBoxDarkMode.IsChecked == true)
            {
                BuildUI_Dark(kerdesek[kerdesIndex]);
            }
            else
            {
                BuildUI(kerdesek[kerdesIndex]);
            }
            if (ellenorizve==true)
            {
                MegoldasGUI();
            }

        }
    }
}
