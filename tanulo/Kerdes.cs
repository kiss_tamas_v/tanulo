﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tanulo
{
    class Kerdes
    { 
        public int ValaszDB { get; private set; }
        public string[] Valaszok { get; set; }//teszt commit

        public List<string> Megoldas { get; private set; }
        public string GetKerdes { get; set; }

        private string[] temp;
        private string[] valaszok;
        public Kerdes(string input)
        {
            temp = input.Split('\n').Where( s => s.Length > 0 && !char.IsWhiteSpace(s[0])).ToArray();
            this.GetKerdes = temp[0];
            //this.ValaszDB = temp.Length - 3;
            this.ValaszDB = temp.Length-1;
            valaszok = new string[ValaszDB];
            this.Megoldas = new List<string>();


            for (int i = 0; i < valaszok.Length; i++)
            {
                valaszok[i] = temp[i + 1];

                if (valaszok[i].Contains("#M"))
                {
                    valaszok[i]=valaszok[i].Substring(2);
                    this.Megoldas.Add(valaszok[i]);
                }                
            }

            this.Valaszok = valaszok;
        }

        public void SorrendVisszaallitas()
        {
            for (int i = 0; i < valaszok.Length; i++)
            {
                valaszok[i] = temp[i + 1];

                if (valaszok[i].Contains("#M"))
                {
                    valaszok[i] = valaszok[i].Substring(2);
                    //this.Megoldas.Add(valaszok[i]);
                }
            }

            this.Valaszok = valaszok;
        }
    }
}
